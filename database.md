# database schema

## Board : 

- title
- short description
- author
- authorised users

[1] : need many-to-many foreign keys relationship with to-do lists




## To-do lists :

- title
- short description
- author
- authorised users
- creation date
- one-to-many foreign key relationship with boards

[2] : need many-to-many foreign key relationship with cards
[3] : views are more important here

    - **view** : select all cards with to-do lists foreign key matching to-do list's primary key
    - **view** : describe modes of cards (1. Extremely short summary, 2. short summary)
    - **view** : sort by a few fields of the card e.g. severity, due date etc
    - **view** : select only items with overdue flag or high severity status


## Cards :

- title
- short description
- urgency/severity
- author
- assigned to
- authorised users
- file attachments
- creation date time
- due date time
- overdue flag
- remarks by each user
- one-to-many foreign key relationship with to-do lists

[4] : cards correspond to individual issues
[5] : owner of higher hierarchy should be automatically added to the list of authorised users. e.g Boards, to-do lists

    - **[sidenote]** : we can use groups for this 
    - **[Sidenote]** : we can create users with permissions selected from a pre-defined list.



## notes to self :
1. take care of sidenotes
2. write views and views' logic for each user and level
3. auto-fill defaults with an option to change
4. make some fields compulsory
5. write flow chart for documentation.

