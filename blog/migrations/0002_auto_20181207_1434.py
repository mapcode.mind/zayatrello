# Generated by Django 2.1.2 on 2018-12-07 09:04

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):
    atomic=False

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Post',
            new_name='Board',
        ),
    ]
