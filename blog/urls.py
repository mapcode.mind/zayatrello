from django.urls import path
from . import views


urlpatterns= [
        path('', views.board_list, name='board_list'),
        path('board/<int:pk>/', views.board_detail, name='board_detail'),
        path('board/new', views.board_new, name='board_new'),
        path('board/<int:pk>/edit/', views.board_edit, name='board_edit'),
        path('drafts/', views.board_draft_list, name='board_draft_list'),
        path('board/<int:pk>/publish/', views.board_publish, name='board_publish'),
        path('board/<int:pk>/remove/',  views.board_remove, name='board_remove'),
        ]
