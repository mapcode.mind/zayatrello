from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Board, list_board, card_list

class SignUpForm(UserCreationForm):
    #first_name= forms.CharField(max_length=30, required=False, help_text='Optional.')
    #last_name= forms.CharField(max_length=30, required=False, help_text='Optional.')
    #email = forms.EmailField(max_length=254, required=False, help_text='you can optionally provide an email address')
    choices = [('premium_user', 'premium'),
            ('free_user','free')]
    acc_type = forms.ChoiceField(choices=choices, widget=forms.RadioSelect)

    class Meta:
        model =User
        fields=('username', 'acc_type','password1','password2',)


class BoardForm(forms.ModelForm):
    class Meta: # meta class tells django which model to use to create this form
        model = Board
        fields = ('title',)

class ListForm(forms.ModelForm):
    class Meta:
        model=list_board
        fields=('list_members', 'title','board_list')

class CardForm(forms.ModelForm):
    class Meta:
        model=card_list
        fields=('card_members','title','content','list_card','due_date', 'attachment')

