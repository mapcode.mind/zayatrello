from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from .models import Board
from .forms import BoardForm, SignUpForm, ListForm, CardForm

from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm

# Create your views here.

@login_required
def board_list(request):
    boards = Board.objects.filter(board_admin__exact=request.user).filter(published_date__lte=timezone.now()).order_by('published_date')
    return render(request,'blog/board_list.html',{'boards':boards})

@login_required
def board_detail(request, pk):
    board=get_object_or_404(Board, pk=pk) 
    if board.board_admin==request.user:
        return render(request, 'blog/board_detail.html', {'board':board})
    else:
        return render(request, 'blog/error.html')

@login_required 
def board_new(request):
    if request.method=='POST':
        form = BoardForm(request.POST)

        if form.is_valid():
            board = form.save(commit=False)
            board.board_admin=request.user
            #board.published_date=timezone.now()
            board.save()
            return redirect('board_detail', pk=board.pk)

    else:
        form = BoardForm()
    return render(request, 'blog/board_edit.html', {'form':form})

@login_required 
def board_edit(request, pk):
    board=get_object_or_404(Board,pk=pk)
    if board.board_admin==request.user:
        if request.method=='POST':
            form = BoardForm(request.POST, instance=board)
            if form.is_valid():
                board = form.save(commit=False)
                board.board_admin= request.user
                #board.published_date=timezone.now()
                board.save()
                return redirect('board_detail', pk=board.pk)

        else:
            form = BoardForm(instance=board)
        return render(request, 'blog/board_edit.html', {'form':form})
    else:
        return render(request, 'blog/error.html')

@login_required
def board_draft_list(request):
    boards = Board.objects.filter(board_admin__exact=request.user).filter(published_date__isnull=True).order_by('created_date')
    return render(request, 'blog/board_draft_list.html', {'boards':boards})

@login_required 
def board_publish(request,pk):
    board = get_object_or_404(Board, pk=pk)
    board.publish()
    return redirect('board_detail', pk=pk)

@login_required
def board_remove(request,pk):
    board= get_object_or_404(Board, pk=pk)
    if board.board_admin==request.user:
        board.delete()
        return redirect('board_list')
    else:
        return render(request, 'blog/error.html')

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username=form.cleaned_data.get('username')
            raw_password=form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/')
    else:
        form = SignUpForm()

    return render(request, 'registration/signup.html', {'form':form})
