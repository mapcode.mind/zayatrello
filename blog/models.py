from django.db import models
from django.conf import settings
from django.utils import timezone

# Create your models here.

#def user_directory_path(instance, filename):
 #   return f'user_{instance.user.id}/{filename}'

class Board(models.Model):
    # need to take care of acc_type field in the view
    board_admin = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null= True)

    def publish(self):
        self.published_date=timezone.now()
        self.save()

    def __str__(self):
        return self.title

class list_board(models.Model):
    list_admin = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    list_members = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='list_members')    
    title=models.CharField(max_length=100)
    board_list=models.ForeignKey(Board, on_delete=models.CASCADE)
    created_date=models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.title

class card_list(models.Model):
    # need to take care of allowing moving only to lists in current board under views
    card_admin= models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    card_members=models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='card_members')
    title=models.CharField(max_length=100)
    content = models.TextField(max_length=300, default="sample card")
    list_card = models.ForeignKey(list_board, on_delete=models.CASCADE) 
    due_date=models.DateTimeField(blank=False, null=False)
    #attachment = models.FileField(upload_to=user_directory_path)
    attachment = models.FileField(upload_to='uploads/')

    def __str(self):
        return self.title


    
